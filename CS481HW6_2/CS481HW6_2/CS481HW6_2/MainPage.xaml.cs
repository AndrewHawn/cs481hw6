﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microcharts;
using SkiaSharp;
using Entry = Microcharts.Entry;
using Xamarin.Forms;

namespace CS481HW6_2
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            CreateCharts();
        }
        private void CreateCharts()
        {
            var entryList = new List<Entry>()
            {
                    new Entry(12000)
                    {
                        Label = "DH Damage",
                        Color = SKColor.Parse("FF00FF"),
                    },
                    new Entry(11000)
                    {
                        Label = "Mage Damage",
                        Color = SKColor.Parse("00FFFF"),
                    },
                    new Entry(8000)
                    {
                        Label = "DK Damage",
                        Color = SKColor.Parse("FF0000"),
                    }
            };
            var barChart = new BarChart()
            {
                Entries = entryList
            };
            var donutChart = new DonutChart()
            {
                Entries = entryList
            };
            var lineChart = new LineChart()
            {
                Entries = entryList
            };
            Chart1.Chart = barChart;
            Chart2.Chart = donutChart;
            Chart3.Chart = lineChart;
            Chart1.IsVisible = false;
            Chart2.IsVisible = false;
            Chart3.IsVisible = false;
        }
        void PickedChart(object sender, EventArgs e)
        {
            var picker = (Picker)sender;
            int selectedIndex = picker.SelectedIndex;

            if (selectedIndex == 0)
            {
                Chart1.IsVisible = !Chart1.IsVisible;
            }
            else if (selectedIndex == 1)
            {
                Chart2.IsVisible = !Chart2.IsVisible;
            }
            else if (selectedIndex == 2)
            {
                Chart3.IsVisible = !Chart3.IsVisible;
            }
        }
    }
}
